import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <h1>Hi people</h1>
    <h2>Aqui haciendo un cambio alv</h2>
    <p>Bienvenidos a mi sitio, estoy haciendo un nuevo merge request.</p>
    <p>Hola prro.</p>
    <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
      <Image />
    </div>
    <Link to="/page-2/">Go to page 2</Link>
    <div>Version: %%VERSION%%</div>
  </Layout>
)

export default IndexPage
